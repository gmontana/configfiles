"    ____      _ __        _
"   /  _/___  (_) /__   __(_)___ ___
"   / // __ \/ / __/ | / / / __ `__ \
" _/ // / / / / /__| |/ / / / / / / /
"/___/_/ /_/_/\__(_)___/_/_/ /_/ /_/

source $HOME/.config/nvim/vimfiles/polyglot.vim

"General settings
source $HOME/.config/nvim/vimfiles/plugins.vim
source $HOME/.config/nvim/vimfiles/settings.vim
source $HOME/.config/nvim/vimfiles/mappings.vim

"Plugin configurations
source $HOME/.config/nvim/vimfiles/sneak.vim
source $HOME/.config/nvim/vimfiles/airline.vim
source $HOME/.config/nvim/vimfiles/which-key.vim
source $HOME/.config/nvim/vimfiles/quickscope.vim
source $HOME/.config/nvim/vimfiles/goyo.vim
source $HOME/.config/nvim/vimfiles/vim-rooter.vim
source $HOME/.config/nvim/vimfiles/tagalong.vim
source $HOME/.config/nvim/vimfiles/start-screen.vim
source $HOME/.config/nvim/vimfiles/denite.vim
" source $HOME/.config/nvim/vimfiles/ultisnips.vim
source $HOME/.config/nvim/vimfiles/vimlatex.vim
source $HOME/.config/nvim/vimfiles/citation.vim
source $HOME/.config/nvim/vimfiles/markdown.vim
source $HOME/.config/nvim/vimfiles/neoformat.vim
source $HOME/.config/nvim/vimfiles/nerdtree.vim
source $HOME/.config/nvim/vimfiles/fzf.vim
source $HOME/.config/nvim/vimfiles/coc.vim
source $HOME/.config/nvim/vimfiles/fugitive.vim
source $HOME/.config/nvim/vimfiles/gitgutter.vim
source $HOME/.config/nvim/vimfiles/gitmessenger.vim
source $HOME/.config/nvim/vimfiles/gutentags.vim
source $HOME/.config/nvim/vimfiles/codi.vim
source $HOME/.config/nvim/vimfiles/floaterm.vim
