
let g:citation_vim_mode="zotero"
let g:citation_vim_zotero_path="/home/gm13/Dropbox/zotero"
" let g:citation_vim_collection" = 'deep learning'
let g:citation_vim_zotero_version=5
let g:citation_vim_et_al_limit=2
let g:citation_vim_bibtex_file='/home/gm13/Dropbox/zoterodb/MyLibrary.bib'
let g:citation_vim_mode="bibtex"
