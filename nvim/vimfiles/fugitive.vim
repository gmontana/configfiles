function! Git_branch()
    let l:branch = fugitive#head()
    return empty(l:branch)?'':'['.l:branch.']'
endfunction
