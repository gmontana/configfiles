" map <silent> <leader>ee :e $HOME/.config/nvim/init.vim<CR>
setl dictionary+=$HOME/.config/nvim/dev.dict

" Basic Key Mappings
imap <C-h> <C-w>h
imap <C-j> <C-w>j
imap <C-k> <C-w>k
imap <C-l> <C-w>l

" g Leader key
let mapleader=" "

" Better indenting
vnoremap < <gv
vnoremap > >gv

" Better nav for omnicomplete
inoremap <expr> <c-j> ("\<C-n>")
inoremap <expr> <c-k> ("\<C-p>")

" I hate escape more than anything else
inoremap jk <Esc>
inoremap kj <Esc>

" TAB in general mode will move to text buffer
nnoremap <silent> <TAB> :bnext<CR>
" SHIFT-TAB will go back
nnoremap <silent> <S-TAB> :bprevious<CR>

map ? /\<\><Left><Left>
map <silent> <leader>n :nohlsearch<CR>

nnoremap <leader>cp :set clipboard=unnamed<CR>
nnoremap <silent>gb :bn<CR>
nnoremap <silent>gB :bp<CR>
nnoremap <leader>w :bd!<CR>

" Move selected line / block of text in visual mode
" shift + k to move up
" shift + j to move down
xnoremap K :move '<-2<CR>gv-gv
xnoremap J :move '>+1<CR>gv-gv

" Alternate way to save
nnoremap <silent> <C-s> :w<CR>
" Alternate way to quit
nnoremap <silent> <C-Q> :wq!<CR>
" Use control-c instead of escape
nnoremap <silent> <C-c> <Esc>
" <TAB>: completion.
inoremap <silent> <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

" Better window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Terminal window navigation
tnoremap <C-h> <C-\><C-N><C-w>h
tnoremap <C-j> <C-\><C-N><C-w>j
tnoremap <C-k> <C-\><C-N><C-w>k
tnoremap <C-l> <C-\><C-N><C-w>l
inoremap <C-h> <C-\><C-N><C-w>h
inoremap <C-j> <C-\><C-N><C-w>j
inoremap <C-k> <C-\><C-N><C-w>k
inoremap <C-l> <C-\><C-N><C-w>l
tnoremap <Esc> <C-\><C-n>

nnoremap <silent> <C-Up>    :resize -2<CR>
nnoremap <silent> <C-Down>  :resize +2<CR>
nnoremap <silent> <C-Left>  :vertical resize -2<CR>
nnoremap <silent> <C-Right> :vertical resize +2<CR>

let g:elite_mode=0                      " Disable arrows"
" Disable arrow movement, resize splits instead.
if get(g:, 'elite_mode')
    nnoremap <C-Up>    :resize -2<CR>
    nnoremap <C-Down>  :resize +2<CR>
    nnoremap <C-Left>  :vertical resize -2<CR>
    nnoremap <C-Right> :vertical resize +2<CR>
endif

" Better nav for omnicomplete
inoremap <expr> <c-j> ("\<C-n>")
inoremap <expr> <c-k> ("\<C-p>")

" Insert mode
inoremap <C-c> <ESC>
inoremap <C-w> <C-[>diwa
inoremap <C-h> <BS>
inoremap <C-d> <Del>

inoremap <C-u> <C-G>u<C-U>
inoremap <C-b> <Left>
inoremap <C-f> <Right>
inoremap <C-a> <Home>
inoremap <C-n> <Down>
inoremap <C-p> <Up>
inoremap <expr><C-e> pumvisible() ? "\<C-e>" : "\<End>"

" Command line mappings
cnoremap <C-a> <Home>
cnoremap <C-b> <S-Left>
cnoremap <C-f> <S-Right>
cnoremap <C-e> <End>
cnoremap <C-d> <Del>
cnoremap <C-h> <BS>
cnoremap <C-t> <C-R>=expand("%:p:h") . "/" <CR>
nmap t<Enter> :bo sp term://zsh\|resize 10<CR>i

tnoremap <Esc> <C-\><C-n>

" Toggle between normal and relative numbering
nnoremap <leader>r :call NumberToggle()<cr>

" Fugitive
" nmap     <leader>gs :Gstatus<CR>gg<c-n>
" nnoremap <leader>gd  :Gdiff<CR>

" Execute Makefile
nnoremap <leader>m :make <CR>

"Insert new lines
nmap <S-Enter> O<Esc>j
nmap <CR> o<Esc>k

" Quickly insert an empty new line without entering insert mode
nnoremap <leader>o o<Esc>
nnoremap <leader>O O<Esc>

" Execute a Python 3 script
nnoremap <leader>e :sp <CR> :term python % <CR>
" nnoremap <leader>c :JupyterSendCell <CR>

" Surround
nmap <leader>' ciw'<c-r>"'<esc>
vmap <leader>' c'<c-r>"'<esc>
nmap <leader>( ciw(<c-r>")<esc>
vmap <leader>( c(<c-r>")<esc>

" Move between windows
nnoremap <C-K> <C-W><C-K>

" Change cursor colors
highlight Cursor guifg=green guibg=green
highlight iCursor guifg=green guibg=green
set guicursor=n-v-c:block-Cursor
set guicursor+=i:ver100-iCursor
set guicursor+=n-v-c:blinkon1
set guicursor+=i:blinkwait1

" Where to split
set splitbelow
set splitright
nnoremap <C-L> <C-W><C-L>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-H> <C-W><C-H>

" Move
nnoremap <C-k> <Up>ddp<Up>
nnoremap <C-j> ddp

" Move to next and previous buffers with CTRL
map <C-H> :bnext<CR>
map <C-L> :bprev<CR>

" History
nnoremap <leader>h :History<CR>

" Move lines up and down quickly
" *********************************************************
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv

" Indentation and folding
" au BufNewFile,BufRead *.py \
" set foldmethod=indent
noremap <leader> za

" Move text
vnoremap <A-k> :m '<-2<CR>gv=gv

" nnoremap <silent><S-Enter> :set paste<CR>m`o<Esc>``:set nopaste<CR>
" nnoremap <silent><CR> :set paste<CR>m`O<Esc>``:set nopaste<CR>
" Highligh characters after 80 characters
" autocmd FileType python setlocal colorcolumn=79
