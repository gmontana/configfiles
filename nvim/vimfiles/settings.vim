
" set directory=/Users/giovannimontana/.tmp
let g:python3_host_prog = expand("~/anaconda3/bin/python3")
let g:python_host_prog = expand("~/anaconda2/bin/python")

set foldmethod=marker

" set noswapfile
set tags+=tags;$HOME

map <Space> <leader>

nnoremap <leader>t :Tags <cr>
nnoremap <leader>tb :Tagbar <cr>
nnoremap <leader b :Buffers <cr>

"set backspace=2   " Backspace deletes like most programs in insert modeline
set backspace=indent,eol,start
set hidden  " allow buffer switching without saving
set ruler
set showcmd
set laststatus=2
scriptencoding utf8
set encoding=UTF-8
set autoindent
set autoread
set complete-=i
set tabstop=4
set shiftwidth=4
set expandtab
set number
set numberwidth=5
set linebreak
set noshowmode " disable extraneous messages
set ruler " show the cursor position all the time
set nobackup
set nowritebackup
set cmdheight=2
set updatetime=100
set shortmess+=c
set gdefault      " Never have to type /g at the end of search / replace again
set ignorecase    " case insensitive searching (unless specified)
set smartcase
set hlsearch
set incsearch
set showmatch
set relativenumber
set rnu
" set nofoldenable
set inccommand=split
set title
set hidden
set pumheight=20
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

nnoremap <silent> <leader>, :noh<cr> " Stop highlight after searching

" Syntax highlighting
syntax on
filetype plugin indent on

set laststatus=2
set noshowmode
set nomodeline

hi Comment cterm=italic
let g:nvcode_termcolors=256

syntax on
" colorscheme nvcode
" colorscheme  nord
colorscheme ayu
" let ayucolor="light"  " for light version of theme
" let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme
" colorscheme onedark
" colorscheme industry
" colorscheme TSnazzy
" colorscheme aurora

" checks if your terminal has 24-bit color support
if (has("termguicolors"))
    set termguicolors
    hi LineNr ctermbg=NONE guibg=NONE
endif

" trasparent background
" hi! Normal ctermbg=NONE guibg=NONE
" hi! NonText ctermbg=NONE guibg=NONE guifg=NONE ctermfg=NONE

let g:spaceline_seperate_style= 'none'
let g:spaceline_colorscheme = 'mach2'

" status line
set statusline=""
set statusline+=%{Git_branch()}
set statusline+=\ "
" filename
set statusline+=%<%f
set statusline+=\ "
" " help/modified/readonly
set statusline+=%h%m%r
" " alignment group
set statusline+=%=
" " start error highlight group
set statusline+=%#StatusLineError#
" " errors from w0rp/ale
" set statusline+=%{LinterStatus()}
" " reset highlight group
set statusline+=%#StatusLine#
set statusline+=\ "
" " line/total lines
set statusline+=L%l/%L
set statusline+=\ "
" " virtual column
set statusline+=C%02v)"
set selection=exclusive

" Toggle Relative numbering
function! NumberToggle()
    if(&relativenumber == 1)
        set nornu
        set number
    else
        set rnu
    endif
endfunc
