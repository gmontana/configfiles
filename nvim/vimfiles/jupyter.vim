
if has('nvim')
    let g:python3_host_prog = '~/opt/anaconda3/bin/'
else
    set pyxversion=3
    " OSX
    set pythonthreedll=/Library/Frameworks/Python.framework/Versions/3.6/Python
endif
