" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    "autocmd VimEnter * PlugInstall
    autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/plugged')

" Startup
Plug 'mhinz/vim-startify'

" Status Line
Plug 'glepnir/galaxyline.nvim'
Plug 'kevinhwang91/rnvimr'

" Neovim in Browser
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }

" Editing and navigation
Plug 'unblevable/quick-scope'
Plug 'benekastah/neomake'
" Plug 'ervandew/supertab'
Plug 'scrooloose/nerdcommenter'
Plug 'justinmk/vim-sneak' "Jump to any location specified by two characters
Plug 'asvetliakov/vim-easymotion' "Easy motion
Plug 'majutsushi/tagbar'
Plug 'jsfaint/gen_tags.vim'
Plug 'romgrk/barbar.nvim' "Better tabline
Plug 'ChristianChiarulli/far.vim' "Find and replace
Plug 'liuchengxu/vim-which-key' " See what keys do like in emacs
Plug 'tpope/vim-repeat'

" Better comments
Plug 'tpope/vim-commentary'
Plug 'suy/vim-context-commentstring'

" Improved visuals
Plug 'deris/vim-shot-f'
Plug 'junegunn/goyo.vim' "Zen mode
Plug 'psliwka/vim-smoothie' "Smooth scroll
Plug 'machakann/vim-highlightedyank'
Plug 'ayu-theme/ayu-vim' " Colors sheme
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Icons
Plug 'kyazdani42/nvim-web-devicons'
Plug 'ryanoasis/vim-devicons'

" Format and align code
Plug 'junegunn/vim-easy-align'
Plug 'alvan/vim-closetag'
Plug 'scrooloose/syntastic'
Plug 'jiangmiao/auto-pairs'
Plug 'sbdchd/neoformat'
Plug 'Yggdroot/indentLine'
" Plug 'valloric/MatchTagAlways'
Plug 'Raimondi/delimitMate'
Plug 'tpope/vim-surround' "Surround
Plug 'airblade/vim-rooter' "Have the file system follow you around
Plug 'godlygeek/tabular'
Plug 'sheerun/vim-polyglot' "Better Syntax Support
Plug 'ludovicchabant/vim-gutentags'
Plug 'AndrewRadev/tagalong.vim' "Auto change html tags

" Autocompletion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Markdown
Plug 'iamcco/mathjax-support-for-mkdp'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }

" Treesitter
Plug 'nvim-treesitter/nvim-treesitter'
Plug 'nvim-treesitter/playground'

" File management
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'yuki-ycino/fzf-preview.vim', { 'branch': 'release', 'do': ':UpdateRemotePlugins' }
Plug 'antoinemadec/coc-fzf'
" Plug 'francoiscabrol/ranger.vim'

" Snippets
" Plug 'Shougo/neosnippet.vim'
" Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'Shougo/neosnippet-snippets'
Plug 'mattn/emmet-vim'

" Interactive code
Plug 'metakirby5/codi.vim'

" Swap windows
" Plug 'wesQ3/vim-windowswap'

" Git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'junegunn/gv.vim'
Plug 'rhysd/git-messenger.vim'

" Latex
Plug 'rafaqz/citation.vim'
Plug 'lervag/vimtex'
Plug 'Konfekt/FastFold'
Plug 'matze/vim-tex-fold'
Plug 'lervag/vimtex'
" Plug 'xuhdev/vim-latex-live-preview'
" Plug 'LaTeX-Box-Team/LaTeX-Box'

" Debugging
Plug 'puremourning/vimspector'
Plug 'szw/vim-maximizer'

" Python
Plug 'bfredl/nvim-ipy'
Plug 'jupyter-vim/jupyter-vim'
Plug 'goerz/jupytext.vim'
Plug 'voldikss/vim-floaterm'
" Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Rust
Plug 'rust-lang/rust.vim'
Plug 'rhysd/rust-doc.vim'

" Pandoc / Markdown
" Plug 'vim-pandoc/vim-pandoc', { 'for': [ 'pandoc', 'markdown' ] }
" Plug 'vim-pandoc/vim-pandoc-syntax', { 'for': [ 'pandoc', 'markdown' ] }

call plug#end()

